<section>
	<main>
		<div class="banner">
			<div class="swiper-banner-princ">
				<div class="swiper-wrapper">
			    <?php
			    $sql = mysqli_query($GLOBALS["db"],"SELECT * FROM banner_principal ORDER BY admin_ordem");
			   	?>
			   	<? while ($linha = mysqli_fetch_assoc($sql)) { ?>
					<div class="swiper-slide">
						<div class="container">
							<div class="col-md-1"></div>
							<div class="col-md-10">
							<?php  if ($ipad == true){}else if ($iphone || $android || $palmpre || $ipod || $berry || $symbian == true){?>
								<div class="col-md-4">
									<div class="box position-banner">
										<div class="verde">
											<div class="imagem-hover">
												<p>
												<?=strip_tags($linha["titulo1"]);?><br/>
												<?=strip_tags($linha["titulo2"]);?><br/>
												<?=strip_tags($linha["titulo3"]);?><br/>
												</p>
												<div class="overlay verde"></div>
											</div>
										</div>
									</div>
								</div>
							<?php }else{?>
								<div class="col-md-4">
									<div class="box position-banner">
										<div class="verde">
											<div class="imagem-hover">
												<?=$linha["titulo1"];?>
												<div class="overlay verde"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="box position-banner">
										<div class="verde-agua">
											<div class="imagem-hover">
												<?=$linha["titulo2"];?>
												<div class="overlay verde-agua"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="box position-banner">
										<div class="azul">
											<div class="imagem-hover">
												<?=$linha["titulo3"];?>
												<div class="overlay azul"></div>
											</div>
										</div>
									</div>
								</div>
							<? } ?>

							</div>
							<div class="col-md-1"></div>
							<div class="clearfix"></div>
						</div>
						<img src="admin/uploads/<?=$linha["imagem"];?>"/>
					</div>
				<? } ?>

				</div>
				<div class="swiper-pagination-banner-princ"></div>
			</div>
		</div>
	</main>

    <?php
    $sql = mysqli_query($GLOBALS["db"],"SELECT * FROM sobre");
    $linha = mysqli_fetch_assoc($sql) ;
    ?>
	<section class="sobre" id="sobre">
		<div class="conteudo">
			<div class="container">
				<h2><?=$linha["titulo"];?></h2>
				<div class="col-md-1 nopadding">
					<img class="quad" src="img/quadrado.jpg" alt="">
				</div>
				<div class="col-md-10">
					<div class="col-md-6">
						<?=$linha["texto"];?>
					</div>
					<div class="col-md-6">
						<?=$linha["texto2"];?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</section>

	<section class="atendimento" id="atendimento">
		<div class="conteudo">
			<div class="container">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<h2>ATENDIMENTO</h2>
				    <?php
				    $sql = mysqli_query($GLOBALS["db"],"SELECT * FROM atendimento ORDER BY admin_ordem");
				   	?>
				   	<? while ($linha = mysqli_fetch_assoc($sql)) { ?>
					<div class="col-md-6">
						<div class="titulo">
							<img src="admin/uploads/<?=$linha["img"];?>" alt="<?=$linha["titulo"];?>">
							<h3><?=$linha["titulo"];?></h3>
							<p>
								<?=$linha["texto"];?>
							</p>
						</div>
					</div>
					<? } ?>

					<div class="clearfix"></div>
				</div>
				<div class="col-md-1"></div>
				<div class="clearfix"></div>
			</div>
		</div>
	</section>

	<section class="parceiros" id="convenios" >
		<div class="conteudo">
			<div class="container">
		    <?php
		    $sql = mysqli_query($GLOBALS["db"],"SELECT * FROM convenios ORDER BY admin_ordem");
		   	?>
		   	<? while ($linha = mysqli_fetch_assoc($sql)) { ?>
				<div class="col-md-3">
					<a href="<?=$linha["link"];?>" target="_blank" >
						<img src="admin/uploads/<?=$linha["img"];?>" alt="<?=$linha["titulo"];?>">
					</a>
				</div>
			<? } ?>
				<div class="clearfix"></div>
			</div>
			<img class="banner" src="img/banner.jpg" alt="">
		</div>
	</section>

	<section class="depoimentos" id="depoimentos">
		<div class="conteudo">
			<div class="container">
				<h2>DEPOIMENTOS</h2>

				<div class="col-md-1"></div>
				<div class="col-md-10">
					
					<div class="swiper-depoimentos">
						<div class="swiper-wrapper">
					    <?php
					    $sql = mysqli_query($GLOBALS["db"],"SELECT * FROM depoimentos ORDER BY admin_ordem");
					   	?>
					   	<? while ($linha = mysqli_fetch_assoc($sql)) { ?>
						<?
							if ($linha["img"]) {
								$img = "admin/uploads/".$linha["img"];
							} else {
								$img = "img/balao.png";
							}
						?>
							<div class="swiper-slide">
								<img src="<?=$img;?>" alt="">
								<div class="nome"><?=$linha["titulo"];?></div>
								<p>
									<?=$linha["texto"];?>
								</p>
							</div>
						<? } ?>
						</div>
						<div class="swiper-pagination-depoimentos"></div>
					</div>

				</div>
				<div class="col-md-1"></div>
				<div class="clearfix"></div>
			</div>
		</div>
	</section>

	<section class="contato" id="contato">
		<div class="conteudo">
			<div class="container">
				<h2>contato</h2>

				<div class="col-md-6">
					<form class="email-envio" action="">
						<label for="nome">*NOME</label>
						<input type="text" id="nome" name="nome">
						<label for="email">*E-MAIL</label>
						<input type="text" id="email" name="email">
						<label for="telefone">*TELEFONE</label>
						<input type="text" id="telefone" class="mask-telefone" name="telefone">
						<label for="mensagem">*MENSAGEM</label>
						<textarea name="mensagem" id="mensagem"></textarea>

						<input type="submit" value="ENVIAR">
					</form>
				</div>
<?php
$sql = mysqli_query($GLOBALS["db"],"SELECT * FROM dados_contato");
$linha = mysqli_fetch_assoc($sql) ;
?>
				<div class="col-md-6">  
					<img src="img/logo.png" class="logo" alt="">
					<h4>HORÁRIO DE FUNCIONAMENTO</h4>
					<div class="col-md-6">
						<p>Segunda a sexta-feira</p>
						<p><?=$linha["horario_semana"];?></p>
					</div>
					<div class="col-md-6">
						<div class="traco-vertical"></div>
						<p>Sábado</p>
						<p><?=$linha["horario_fds"];?></p>
					</div>
					<div class="clearfix"></div>

					<h4>ENDEREÇO</h4>
					<div class="col-md-12">
						<img src="img/icon-casa.png" alt="">			
						<span><?=nl2br($linha["endereco"]);?></span>
					</div>
					<div class="clearfix"></div>

					<div class="col-md-12">
							
						<img src="img/icon-telc.png" alt="">			
						<span><?=$linha["telefone"];?></span><br/>
						<img src="img/icon-wppc.png" alt="">			
						<span><?=$linha["whatsapp"];?></span>
							
						<!--
						<div class="col-md-6">
							<img src="img/icon-wppc.png" alt="">			
							<span><?=$linha["whatsapp"];?></span>
						</div>
					-->
						<div class="clearfix">
					</div>
					<div class="clearfix"></div>

					<div class="col-md-12 nopadding">
						<img src="img/icon-email.png" alt="">			
						<span><?=$linha["email"];?></span>
					</div>
					<div class="clearfix"></div>

					<div class="col-md-12 nopadding">
						<div class="col-md-6 nopadding">
							<img src="img/icon-insta.png" alt="">
							<a href="http://instagram.com/<?=$linha["instagram"];?>" target="_blank">			
								<span><?=$linha["instagram"];?></span>
							</a>
						</div>
						<div class="col-md-6">
							<img src="img/icon-fb.png" alt="">
							<a href="http://facebook.com/<?=$linha["facebook"];?>" target="_blank">		
								<span><?=$linha["facebook"];?></span>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>	

				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</section>

	<section class="mapa">
		<div class="conteudo">
			<div class="container">
				<div style="width: 100%"><iframe width="100%" height="350" src="https://maps.google.com/maps?width=100%&amp;height=350&amp;hl=en&amp;q=Rua%20Santa%20Rita%20Dur%C3%A3o%2C%2089%2C%20Funcion%C3%A1rios.%20%20Belo%20Horizonte-MG+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=15&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/coordinates.html">find my coordinates</a></iframe></div><br />
			</div>
		</div>
	</section>
</section>

