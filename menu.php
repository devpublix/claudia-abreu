<?php
$sql = mysqli_query($GLOBALS["db"],"SELECT * FROM dados_contato");
$linha = mysqli_fetch_assoc($sql) ;
?>
<?php  if ($ipad == true){}else if ($iphone || $android || $palmpre || $ipod || $berry || $symbian == true){?>
<div class="top-mob">    
    <a href="#home" class="scroll">
        <img src="img/logo.png" class="img-responsive" style="display: inline-block; width: 30%;">
    </a>
    <div class="traco-mobile"></div>
</div>
<div class="clearfix"></div>
    
<div class="menu-mobile">
    <div class="logo-mobile">
        <div class="col-xs-12 nopadding">
            <div class="espaco">
                <div class="col-xs-2 nopadding">
                    <img src="img/fecha.png" style="float: left; width: 54px; padding: 15px;">
                </div>
                <div class="col-xs-8 nopadding">
                    <a href="#home"  class="scroll">
                        <img src="img/logo.png">
                    </a> 
                </div>
                <div class="col-xs-2 nopadding"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <nav class="menu-mobile-itens">
            <div class="itens">
                <nav>
                    <ul style="position: static;">
                        <li><a href="#home" class="scroll">HOME</a></li>
                        <li><a href="#sobre" class="scroll">SOBRE</a></li>
						<li><a href="#atendimento" class="scroll">ATENDIMENTO</a></li>
                        <li><a href="#convenios" class="scroll">CONVÊNIOS</a></li>
						<li><a href="#depoimentos" class="scroll">DEPOIMENTOS</a></li>
						<li><a href="#contato" class="scroll">CONTATO</a></li>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
</div>

<!-- =======================================DESKTOP=========================================== -->
   
<?php }else{?>

<div id="menu-fixo" class="menu-fixo">
    <div class="topo">
        <div class="col-md-12">
            <div class="container">
                <div class="col-md-4 col-sm-4">
                    <div class="logo">
                        <a href="#home"  class="scroll">
                            <img src="img/logo.png" class="img-responsive logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="menu-f">
                        <ul class="fix">
                           <li><a href="#home" class="scroll">HOME</a></li>
									<li><a href="#sobre" class="scroll">SOBRE</a></li>
									<li><a href="#atendimento" class="scroll">ATENDIMENTO</a></li>
									<li><a href="#convenios" class="scroll">CONVÊNIOS</a></li>
									<li><a href="#depoimentos" class="scroll">DEPOIMENTOS</a></li>
									<li><a href="#contato" class="scroll">CONTATO</a></li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>  
            </div>
        </div>
    </div>
</div>

<section class="menu">
	<div class="barra-superior">
		<div class="container">
			<div class="col-md-4">
				<a href="#home"  class="scroll"><img src="img/logo.png" class="img-responsive logo-menu"></a>
			</div>
			<div class="col-md-8">
				<div class="box-barra">
					<div class="iniline">
                        <a href="https://api.whatsapp.com/send?phone=5531996519209&amp;text=Ol%C3%A1%2C%20Dra.%20Cl" target="_blank">
						  <img class="altura" src="img/icon-wpp.png" alt="">
                        </a>
					</div>
					<div class="iniline">
						<div class="traco"></div>
					</div>
					<div class="iniline">
						<img class="altura" src="img/icon-tel.png" alt="">
					</div>
					<div class="iniline">
						<div class="altura telefone">
                                <?=$linha["telefone"];?>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<div class="container">
		<div class="col-md-12 altura-menu">
				<div class="col-md-12 nopadding">
					<ul class="menu-conteudo">
						<li><a href="#home" class="scroll">HOME</a></li>
						<li><a href="#sobre" class="scroll">SOBRE</a></li>
						<li><a href="#atendimento" class="scroll">ATENDIMENTO</a></li>
						<li><a href="#convenios" class="scroll">CONVÊNIOS</a></li>
						<li><a href="#depoimentos" class="scroll">DEPOIMENTOS</a></li>
						<li><a href="#contato" class="scroll">CONTATO</a></li>
					</ul>
				</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<? } ?>
<div class="clearfix"></div>