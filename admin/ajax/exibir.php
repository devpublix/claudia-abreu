<?php
	require_once("../verifica_login.php");
	require_once("../config.php");
	require_once("../funcoes.php");
	$table = @$_POST["table"];
	$exibir = @$_POST["exibir"];
	$pag = @$_POST["pag"];
	$id_externo = @$_POST["id_externo"];
	$table_externo = @$_POST["table_externo"];
	$colunas = @$_POST["colunas"];
	$termos = @$_POST["termos"];
	$nivel = @$_POST["nivel"];
	$ordem_campo = @$_POST["ordem_campo"];
	$ordem_modo = @$_POST["ordem_modo"];

	if(isset($_SESSION["nivel"][$nivel])) {
		$add_hash = explode("#",$_SESSION["nivel"][$nivel]);
		$_SESSION["nivel"][$nivel] = $add_hash[0]."#".$table;
	}

	if($pag < 0) { $pag = 0; }

	if(isset($_POST["exibir"]) and $_POST["exibir"] != "" and $_POST["exibir"] != "undefined") {
		if(isset($_COOKIE["exibir_".$table])) {
			if($_COOKIE["exibir_".$table] != $exibir) {
				setcookie("exibir_".$table,$exibir,3600*time());
			}
		} else {
			setcookie("exibir_".$table,$exibir,3600*time());
		}
	} else {
		if(isset($_COOKIE["exibir_".$table])) {
			$exibir = $_COOKIE["exibir_".$table];
		}
	}
?>

<?php
$sql_config = mysqli_query($GLOBALS["db"], "SELECT * FROM admin_configuracoes WHERE tabela = '".$table."' and campo = ''") or die(mysqli_error($GLOBALS["db"]));;
if(mysqli_num_rows($sql_config) != 0) {
	$config = mysqli_fetch_array($sql_config);
	$editavel = $config["editavel"];
	if($config["label"] != "") { $nome = $config["label"]; }

	$orderby = "id ASC";
	if(possui_ordem($table)) { $orderby = "admin_ordem ASC"; }
	if(isset($config["orderby"]) and $config["orderby"] != "") { $orderby = $config["orderby"]; }

}

if($ordem_campo == "" && $ordem_modo == "") {
	$orderby_ex = explode(" ",$orderby);
	$ordem_campo = $orderby_ex[0];
	$ordem_modo = strtolower($orderby_ex[1]);
}

$sql_config = mysqli_query($GLOBALS["db"], "SELECT campo,label FROM admin_configuracoes WHERE tabela = '".$table."' and visivel = 1") or die(mysqli_error($GLOBALS["db"]));;
if(mysqli_num_rows($sql_config) != 0) {
	$campos_visiveis = array();
	$total_campos_visiveis = 0;
	?>
	<thead>
		<tr>
		<?
			$sql = "show full columns from $table";
			$result = mysqli_query($GLOBALS["db"],$sql) or die(mysqli_error($GLOBALS["db"]));
			while($col = mysqli_fetch_array($result)) {
				$sql_config = mysqli_query($GLOBALS["db"], "SELECT campo,label FROM admin_configuracoes WHERE tabela = '".$table."' and campo = '".$col["Field"]."' and visivel = 1") or die(mysqli_error($GLOBALS["db"]));
				while($config = mysqli_fetch_array($sql_config)) {
					$field = $config["campo"];
					$nome = normalizaString($field);

					array_push($campos_visiveis,$field);
					$total_campos_visiveis++;

					if($config["label"] != "") { $nome = $config["label"]; }

					$seta = "";
					$ordem_modo_contrario = "asc";
					if($ordem_campo == $field) {
						if($ordem_modo == "" or $ordem_modo == "desc") { $ordem_modo_contrario = "asc"; $seta = "<i class='zmdi zmdi-chevron-up'></i> "; };
						if($ordem_modo == "asc") { $ordem_modo_contrario = "desc"; $seta = "<i class='zmdi zmdi-chevron-down'></i> "; };
					}

					?>
					<td><a campo="<?=$field?>" modo="<?=$ordem_modo_contrario?>" tabela="<?=$table?>" class="ordenar-titulo"><?=$seta?><?=$nome?></a></td>
				<? } ?>
		<?
			}
		?>
			<td></td>
		</tr>
	</thead>

	<tbody>
		<?

		$leftjoin = "";
		global $dbname;
		$externo_sql = mysqli_query($GLOBALS["db"], "select table_name from information_schema.columns where table_schema = '$dbname' and column_name = 'id_".$table."'") or die(mysqli_error($GLOBALS["db"]));
		while($tb_name = mysqli_fetch_array($externo_sql)) {
			$tb_externo = $tb_name["table_name"];
			$leftjoin .= " LEFT JOIN $tb_externo ON $tb_externo.id_$table = $table.id ";
		}

		$where = "WHERE ".$table.".id != ''";
		if($id_externo != "") {
			$where.= " and ".$table.".id_".$table_externo." = ".$id_externo;
		}

		//Não exibe desenvolvedor
		if($table == "admin_usuarios") {
			$where.=" and id != 1";
		}

		//BUSCA
		if($colunas != null and $termos != null) {
			for($i = 0; $i < sizeof($colunas); $i++) {
				if(strpos($colunas[$i],"admin_ordem") !== false) { $termos[$i] = $termos[$i] - 1; }
				$where .= " and ".$colunas[$i]." LIKE '%".$termos[$i]."%'";
			}
		}

		$total_sql = mysqli_query($GLOBALS["db"], "SELECT $table.id FROM $table $leftjoin $where GROUP BY $table.id") or die(mysqli_error($GLOBALS["db"]));
		$total = mysqli_num_rows($total_sql);

		if($total > 0) {

			if($exibir == "todos") { $exibir = $total; }

			if($exibir == "") { $pags = 1; } else { $pags = ceil($total / $exibir); }

			if(isset($_SESSION["ultima-pag"]) and $_SESSION["ultima-pag"] == 1) {
				$pag = $pags-1;
			}

			$inicia_em = $pag * $exibir;

			if($ordem_campo != "" && $ordem_modo != "") {
				$orderby = " $ordem_campo $ordem_modo";
			}
			
			$sql_result = mysqli_query($GLOBALS["db"], "SELECT $table.* FROM $table $leftjoin $where GROUP BY $table.id ORDER BY $orderby LIMIT $inicia_em,$exibir") or die(mysqli_error($GLOBALS["db"]));

			while($dados = mysqli_fetch_array($sql_result)) {
				?>
				<tr id="linha-<?=$dados["id"]?>">
				<?
				foreach($campos_visiveis as $v) {
				   $add = "";
				   $exibir_dado = $dados[$v];

				   $campo_config = mysqli_query($GLOBALS["db"], "SELECT * FROM admin_configuracoes WHERE tabela = '$table' and campo = '$v'");
				   $campo_config = mysqli_fetch_array($campo_config);

				   if($campo_config["tipo"] == "select") {
				   		if(strpos($campo_config['configuracao'],'&') === false) {
				   			$select_table_field = explode("|",$campo_config['configuracao']);
				   			$select_sql = mysqli_query($GLOBALS["db"], "SELECT ".$select_table_field[1]." FROM ".$select_table_field[0]." WHERE id = '".$dados[$v]."'");
				   			if(mysqli_num_rows($select_sql) != 0) { $exibir_dado = mysqli_result($select_sql,0,$select_table_field[1]); }
				   		}
				   }

				   if($campo_config["tipo"] == "dinheiro") {
				   		$exibir_dado = number_format($exibir_dado,2,",",".");
				   }

				   if($campo_config["tipo"] == "data") {
				   		$exibir_dado = formata_data_site($exibir_dado);
				   }

				   if($campo_config["tipo"] == "datahora") {
				   		$dt = explode(" ",$exibir_dado);
				   		$exibir_dado = formata_data_site($dt[0])." ".$dt[1];
				   }

				   if($campo_config["tipo"] == "imagem") {
				   		$exibir_dado = "<img src='uploads/p_".$exibir_dado."' width='60' />";
				   		$add = "style='background: #000; display: inline-block;'";
				   }


				   if(isset($dados[$v])) {
				   	//Admin_ordem começa do 1 e não do 0
				   	if($v == "admin_ordem") { $exibir_dado +=1; }
				?>
					<td><span <?=$add?>><?=$exibir_dado?></span></td>
				<? } } ?>
					<td class="acoes">
						<? if(tem_permissao($table,"ordenar")) { ?>
						<div class="ordenar-spot"><i class="glyphicon glyphicon-triangle-right"></i><span></span></div>
						<? } ?>

						<? if(tem_permissao($table,"editar")) { ?>
						<a class="btn btn-warning btn-xs" href="index.php?pag=tabela_campos&t=<?=$table?>&id=<?=$dados["id"]?><? if($id_externo != "") { echo "&id_externo=".$id_externo; }  ?><? if($table_externo != "") { echo "&table_externo=".$table_externo; }  ?><? if(@$_POST["nivel"] != "") { echo "&nivel=".($_POST["nivel"]+1); } else { echo "&nivel=1"; } ?>"><i class="glyphicon glyphicon-edit"></i> </a>
						<? } ?>

						<? if(tem_permissao($table,"excluir")) { if($table != 'seo') { ?>
						<a class="btn btn-danger btn-xs" onclick="deletar($(this),'<?=$table?>','<?=$dados["id"]?>');"> <i class="glyphicon glyphicon-remove"></i> </a>
						<? } } ?>

						<? if(tem_permissao($table,"ordenar")) { ?>
						<? if(possui_ordem($table)) { ?><a class="btn btn-info btn-xs" onclick="ordenacao_iniciar('<?=$table?>','<?=$dados["id"]?>')"> <i class="glyphicon glyphicon-sort"></i></a><? } ?>
						<? } ?>
					</td>

				</tr>
			<? } ?>

		<? } else { ?>

			<tr>
				<td colspan="<?=$total_campos_visiveis+1?>" align="center">Nenhuma informação cadastrada.</td>
			</tr>

		<? } ?>

	</tbody>

<? } else { ?>
	<tbody>
		<tr>
			<td>Nenhum campo foi configurado como visível.</td>
		</tr>
	</tbody>
<? } ?>

<paginacao />

<? if(isset($pags) and $pags != 1) { ?>

<? for($i = 0; $i < $pags; $i++) { ?>
<li <? if(isset($pags) and $pag == $i) { echo "class='active'"; } ?>><a onclick="exibir('<?=$table?>',$('#exibir_<?=$table?>').val(),'<?=$i?>',<? if($id_externo != "") { echo $id_externo; } else { echo "null"; } ?>,<? if($table_externo != "") { echo "'".$table_externo."'"; } else { echo "null"; } ?>)"><?=$i+1?></a></li>
<? } ?>

<? } ?>

<paginacao />
exibir('<?=$table?>',$('#exibir_<?=$table?>').val(),'0',<? if($id_externo != "") { echo $id_externo; } else { echo "null"; } ?>,<? if($table_externo != "") { echo "'".$table_externo."'"; } else { echo "null"; } ?>)

<? $_SESSION["ultima-pag"] = false; ?>