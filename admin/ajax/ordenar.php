<?php
	require_once("../verifica_login.php");
	require_once("../config.php");
	require_once("../funcoes.php");

	$table = $_POST["table"];
	$posicao_original = $_POST["posicao_original"];
	$posicao_nova = $_POST["posicao_nova"];

	$posicao_nova_array = explode("-",$posicao_nova);
	$posicao_nova_id = $posicao_nova_array[0];
	$posicao_nova_pos = $posicao_nova_array[1];

	//Verifica se as ordens estão zeradas
	$sql = mysqli_query($GLOBALS["db"], "SELECT admin_ordem FROM $table WHERE admin_ordem != 0");
	if(mysqli_num_rows($sql) == 0) {
		//Tabela está com posições zeradas, então distribui ordens
		mysqli_query($GLOBALS["db"], "SELECT @i:=-1");
		mysqli_query($GLOBALS["db"], "UPDATE $table SET admin_ordem = @i:=@i+1;") or die(mysqli_error($GLOBALS["db"]));
	}

	//Pega ordem original
	$sql = mysqli_query($GLOBALS["db"], "SELECT admin_ordem FROM $table WHERE id = '$posicao_original'") or die(mysqli_error($GLOBALS["db"]));
	$ordem_original = mysqli_result($sql,0,"admin_ordem");

	//Pega ordem nova
	$sql = mysqli_query($GLOBALS["db"], "SELECT admin_ordem FROM $table WHERE id = '$posicao_nova_id'") or  die(mysqli_error($GLOBALS["db"]));
	$ordem_nova = mysqli_result($sql,0,"admin_ordem");

	//REALIZA OS UPDATES

	if($posicao_nova_pos == "antes") {
		//Aumenta todas as posições a partir da nova ordem
		mysqli_query($GLOBALS["db"], "UPDATE $table SET admin_ordem = admin_ordem+1 WHERE admin_ordem >= $ordem_nova");

		//Insere o elemento na posição desejada
		mysqli_query($GLOBALS["db"], "UPDATE $table SET admin_ordem = $ordem_nova WHERE id = '$posicao_original'");

		//Elimina buraco deixado pela saída do elemento original
		mysqli_query($GLOBALS["db"], "UPDATE $table SET admin_ordem = admin_ordem-1 WHERE admin_ordem > $ordem_original");

	}

	if($posicao_nova_pos == "depois") {
		//Aumenta todas as posições a partir de ACIMA da nova ordem
		mysqli_query($GLOBALS["db"], "UPDATE $table SET admin_ordem = admin_ordem+1 WHERE admin_ordem > $ordem_nova");

		//Insere o elemento na posição desejada $ordem_nova+1 porque é DEPOIS
		mysqli_query($GLOBALS["db"], "UPDATE $table SET admin_ordem = $ordem_nova+1 WHERE id = '$posicao_original'");

		//Elimina buraco deixado pela saída do elemento original
		mysqli_query($GLOBALS["db"], "UPDATE $table SET admin_ordem = admin_ordem-1 WHERE admin_ordem > $ordem_original");

	}

?>