<form action="tabela_acoes.php" method="POST" enctype="multipart/form-data">

<? include("tipos_de_campos.php"); ?>

<div class="table-responsive">
	<table class="table table-striped table-configuracoes" width="100%">

		<thead>
			<td>Campo</td>
			<td><span title="Editável" onclick="todos_editaveis($(this))" class="glyphicon glyphicon-edit"></span></td>
			<td><span title="Visível" onclick="todos_visiveis($(this))" class="glyphicon glyphicon-eye-open"></span></td>
			<td><span title="Obrigatório" onclick="todos_obrigatorios($(this))" class="glyphicon glyphicon-asterisk"></span></td>
			<td><span title="Pesquisável" onclick="todos_pesquisaveis($(this))" class="glyphicon glyphicon-search"></span></td>
			<td><span title="Campo único" onclick="todos_pesquisaveis($(this))" class="zmdi zmdi-collection-item-1"></span></td>
			<td>Tamanho</td>
			<td>Tipo</td>
			<td>Label</td>
		</thead>

		<tbody>
			<?
				$table = $_GET["table"];
				$sql = "show full columns from $table";
				$result = mysqli_query($GLOBALS["db"],$sql) or die(mysqli_error($GLOBALS["db"]));

				$count = 0;
				while($col = mysqli_fetch_array($result)) {
				$field = $col["Field"];
				$nome = normalizaString($field);

				$sql_recupera = mysqli_query($GLOBALS["db"], "SELECT * FROM admin_configuracoes WHERE tabela = '$table' and campo = '$field'");
				$dados = mysqli_fetch_array($sql_recupera);

				if($dados["label"] != "") {
					$nome = $dados["label"];
				}

				?>
				<tr>
					<td><?=$field?></td>
					<td align="center"><input <? if($dados["editavel"] == 1) { echo "checked"; } ?> name="editavel" onchange="altera_campo($(this))" table="<?=$table?>" campo="<?=$field?>" type="checkbox"></td>
					<td align="center"><input <? if($dados["visivel"] == 1) { echo "checked"; } ?> name="visivel" onchange="altera_campo($(this))" table="<?=$table?>" campo="<?=$field?>" type="checkbox"></td>
					<td align="center"><input <? if($dados["obrigatorio"] == 1) { echo "checked"; } ?> name="obrigatorio" onchange="altera_campo($(this))" table="<?=$table?>" campo="<?=$field?>" type="checkbox"></td>
					<td align="center"><input <? if($dados["pesquisavel"] == 1) { echo "checked"; } ?> name="pesquisavel" onchange="altera_campo($(this))" table="<?=$table?>" campo="<?=$field?>" type="checkbox"></td>
					<td align="center"><input <? if($dados["unico"] == 1) { echo "checked"; } ?> name="unico" onchange="altera_campo($(this))" table="<?=$table?>" campo="<?=$field?>" type="checkbox"></td>
					<td>

						<select class="form-control" autocomplete="off" name="tamanho" table="<?=$table?>" campo="<?=$field?>" onchange="altera_campo($(this))">
						<? for($i = 1; $i <= 12; $i++) { ?>
							<option <? if($dados["tamanho"] == $i) { echo "selected"; } else { if($i == 12 and $dados["tamanho"] == "") { echo "selected"; } } ?> value="<?=$i?>"><?=$i?></option>
						<? } ?>
						</select>
					</td>
					<td>
						<span class="relativo">
							<select class="form-control" autocomplete="off" config="<?=$dados["configuracao"]?>" name="tipo" table="<?=$table?>" campo="<?=$field?>" onchange="altera_campo($(this));tipo_opcoes('<?=$table?>','<?=$field?>',this.value,$(this))">
							<? foreach($tipos_de_campos as $tipo) { ?>
								<option <? if($dados["tipo"] == $tipo) { echo "selected"; } ?> value="<?=$tipo?>"><?=$tipo?></option>
							<? } ?>
							</select>
						</span>
					</td>
					<td>
						<span>
							<input class="form-control" autocomplete="off" name="label" type="text" value="<?=$nome?>" table="<?=$table?>" campo="<?=$field?>" onblur="altera_campo($(this))" />
							<div class="alteracao-salva">SALVO</div>
						</span>
					</td>
				</tr>
				<?
				 $count++;
				}
			?>
		</tbody>
	</table>
</div>

</form>

<!-- JANELAS MODAIS -->

<!-- SELECT -->
<div id="tipo-select" class="modal fade" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<div class="panel panel-primary">
			<div class="panel-heading">Configurações de Select <a data-dismiss="modal" class="fechar"><i class="glyphicon glyphicon-remove"></i></a></div>
			<div class="panel-body">

				<form method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-12 ativo">
							<label>
								<span>
									<input class="check" autocomplete="off" checked type="radio" name="metodo" onclick="ativa_campos($(this).parent().parent().parent()); desativa_campos($(this).parent().parent().parent().next().next());" />
									Digite as opções separando-as por &
								</span>
								<input class="form-control" name="config1" type="text" />
							</label>
						</div>
						<div class="clearfix"></div>
						<div class="linha"><span>OU</span></div>
						<div class="clearfix"></div>
						<div class="col-md-12 inativo">
							<label>
								<span>
									<input class="uncheck" autocomplete="off" type="radio" name="metodo" onclick="ativa_campos($(this).parent().parent().parent()); desativa_campos($(this).parent().parent().parent().prev().prev());" />
									Buscar da tabela:
								</span>
								<select class="form-control" name="config1" disabled="disabled" autocomplete="off" onchange="pega_campos_select(this.value,$(this))">
									<option value="">Escolha...</option>
									<?php
										$sql = "SHOW full TABLES FROM $dbname";
										$result = mysqli_query($GLOBALS["db"],$sql) or die(mysqli_error($GLOBALS["db"]));
										while($tables = mysqli_fetch_array($result)) {
									?>
										<option><?=$tables[0]?></option>
									<? } ?>
								</select>
							</label>

							<label style="display: none;">
								<span>Exibindo como opção o campo:</span>
								<select class="form-control" name="config2" disabled="disabled" autocomplete="off" value="">
									<?php
										$sql = "SHOW full TABLES FROM $dbname";
										$result = mysqli_query($GLOBALS["db"],$sql) or die(mysqli_error($GLOBALS["db"]));
										while($tables = mysqli_fetch_array($result)) {
											$sql2 = "show full columns from ".$tables[0];
											$result2 = mysqli_query($GLOBALS["db"],$sql2) or die(mysqli_error($GLOBALS["db"]));
											while($col = mysqli_fetch_array($result2)) {
										?>
											<option class="opt <?=$tables[0]?>"><?=$col["Field"]?></option>
										<? } } ?>
								</select>
							</label>

						</div>

						<div class="col-md-12">
							<a onclick="salvar_config($(this))" class="salvar btn btn-primary">SALVAR</a>
							<a data-dismiss="modal" class="salvar btn btn-danger aright">CANCELAR</a>
						</div>
					</div>
					<input type="hidden" value="" name="campo" />
					<input type="hidden" value="<?=$_GET["table"]?>" name="table" />
				</form>
			</div>
		</div>
	</div>

</div>

<!-- IMAGEM -->
<div id="tipo-imagem" class="modal fade" role="dialog" data-backdrop="static">

	<div class="modal-dialog">
		<div class="panel panel-primary">
			<div class="panel-heading">Configurações de Imagem <a data-dismiss="modal" class="fechar"><i class="glyphicon glyphicon-remove"></i></a></div>
			<div class="panel-body">
				<form method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-6 ativo column">
							<label>
								<span>Digite a largura:</span>
								<input class="form-control" name="config1" type="number" />
							</label>
						</div>

						<div class="col-md-6 ativo column">
							<label>
								<span>Digite a altura:</span>
								<input class="form-control" name="config2" type="number" />
							</label>
						</div>

						<div class="col-md-12 ativo">
							<label>
								<span>Formatos aceitos (separados por vírgula):</span>
								<input class="form-control" name="config3" type="text" value="jpg,jpeg,png,gif" />
							</label>
						</div>

						<div class="col-md-12">
							<a onclick="salvar_config($(this))" class="salvar btn btn-primary">SALVAR</a>
							<a data-dismiss="modal" class="salvar btn btn-danger aright">CANCELAR</a>
						</div>
					</div>
					<input type="hidden" value="" name="campo" />
					<input type="hidden" value="<?=$_GET["table"]?>" name="table" />
				</form>
			</div>
		</div>
	</div>

</div>

<!-- ARQUIVO -->
<div id="tipo-arquivo" class="modal fade" role="dialog" data-backdrop="static">

	<div class="modal-dialog">
		<div class="panel panel-primary">
			<div class="panel-heading">Configurações de Imagem <a data-dismiss="modal" class="fechar"><i class="glyphicon glyphicon-remove"></i></a></div>
			<div class="panel-body">

				<form method="POST" enctype="multipart/form-data">
					<div class="row">

						<div class="col-md-12 ativo">
							<label>
								<span>Formatos aceitos (separados por vírgula):</span>
								<input class="form-control" name="config1" type="text" value="" />
						</label>
						</div>

						<div class="col-md-12">
							<a onclick="salvar_config($(this))" class="salvar btn btn-primary">SALVAR</a>
							<a data-dismiss="modal" class="salvar btn btn-danger aright">CANCELAR</a>
						</div>

					</div>
					<input type="hidden" value="" name="campo" />
					<input type="hidden" value="<?=$_GET["table"]?>" name="table" />
				</form>

			</div>
		</div>
	</div>

</div>
