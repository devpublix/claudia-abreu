<?php
$table = $_GET["t"];

$liberado = true;

if(isset($_GET["id"])) {
	$id = $_GET["id"];

	if(!tem_permissao($table,"editar")) {
		$liberado = false;
	}
} else {
	$id = null;

	if(!tem_permissao($table,"cadastrar")) {
		$liberado = false;
	}
}

$um_registro = 0;
$nome_table = normalizaString($table);
$config_table = mysqli_query($GLOBALS["db"], "SELECT * FROM admin_configuracoes WHERE tabela = '$table' and campo = ''");
if(mysqli_num_rows($config_table) != 0) {
	$tb_config = mysqli_fetch_array($config_table);
	if($tb_config["label"] != "") { $nome_table = $tb_config["label"]; }
	if($tb_config["um_registro"]) { $um_registro = 1; }
}

$_SESSION['nivel'][$nivel] = $REQUEST_PROTOCOL."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>

<?php
$tabelas_externas = array();
$tabelas_externas[$table] = $nome_table;

$externo = false;
//Verifica se tabelas externas referenciam
$externo_sql = mysqli_query($GLOBALS["db"], "select table_name from information_schema.columns where table_schema = '$dbname' and column_name = 'id_".$table."'") or die(mysqli_error($GLOBALS["db"]));
if(mysqli_num_rows($externo_sql) != 0) { $externo = true; }
if($externo) {
	while($ext= mysqli_fetch_array($externo_sql)) {
		$table_name = $ext["table_name"];
		$nome_table_ext = normalizaString($table_name);
		$config_table_ext = mysqli_query($GLOBALS["db"], "SELECT * FROM admin_configuracoes WHERE tabela = '".$table_name."' and campo = ''");
		if(mysqli_num_rows($config_table_ext) != 0) {
			$tb_config_ext = mysqli_fetch_array($config_table_ext);
			if($tb_config_ext["label"] != "") { $nome_table_ext = $tb_config_ext["label"]; }
		}

		if($tb_config_ext["editavel_aba"] == 1) {
			if(tem_permissao($nome_table_ext,'acessar')) {
				$tabelas_externas[$table_name] = $nome_table_ext;
			}
		}
	}
}


$campo_principal = pega_campo_principal($table,$id);;
?>
<? $_SESSION['bread'][$nivel] = $campo_principal; ?>

<? if(!$um_registro) { ?>
	<h1>
		<? if($id != null) { ?>
			<? if($tb_config["icone"]) { ?><i class="<?=$tb_config["icone"]?>"></i><? } ?>
			<?=$campo_principal?>
		<? } else { ?>
			<? if($tb_config["icone"]) { ?><i class="<?=$tb_config["icone"]?>"></i><? } ?> Adicionar: <?=$nome_table?>
		<? } ?>
	</h1>

	<? if(isset($_SESSION["nivel"][$nivel-1])) { ?>
		<a href="<?=$_SESSION["nivel"][$nivel-1]?>" id="btn-voltar" class="btn btn-primary aright btn-top btn-bot icone"><i class="glyphicon glyphicon-arrow-left"></i> VOLTAR</a>
	<? } ?>
<? } ?>

<? if($um_registro) { ?>
	<br />
<? } ?>

<div class="clearfix"></div>
<? if(!$um_registro) { ?>
	<ol class="breadcrumb">
		<? if(isset($_SESSION["bread"])) {
			if($id != null) { $add = 1; } else { $add = 0; }
			for($i = 0; $i < $nivel+$add; $i++) {
				echo "<li><a href='".$_SESSION["nivel"][$i]."'>";
				echo retira_prefixo($_SESSION["bread"][$i]);
				echo "</a></li>";
			}
		} ?>
	</ol>
<? } ?>

<input type="hidden" class="nivel" value="<?=$nivel?>" id="nivel">

<? if($liberado) { ?>
	<ul class="nav nav-tabs escondido">
		<? $count = 0;
		foreach($tabelas_externas as $k=>$v) {
			?>
			<li class="<? if($count == 0) { ?>active<? } ?>">
				<? if($id == null and $count > 0) { ?>
					<a class="tab-item desativado" href="#"><?=retira_prefixo($v)?></a>
				<? } else { ?>
					<a class="tab-item" data-toggle="tab" href="#<?=$k?>" onclick="exibir('<?=$k?>',$('#exibir_<?=$k?>').val(),'0',<? if(isset($_GET["id"])) { echo "'".$_GET["id"]."'"; } else { echo 'null'; } ?>,<? if(isset($_GET["t"])) { echo "'".$_GET["t"]."'"; } else { echo 'null'; } ?>)"><?=retira_prefixo($v)?></a>
				<? } ?>
			</li>
			<? $count++; } ?>
	</ul>


	<div class="tab-content escondido">
		<?
		crud_tabela($table,$id);
		?>
		<div class="clearfix"></div>
	</div>
<? } ?>

<!-- MODAL CROP -->
<div id="crop-imagem" class="modal fade" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<div class="panel panel-primary">
			<div class="panel-heading">Recortar Imagem <a data-dismiss="modal" class="fechar"><i class="glyphicon glyphicon-remove"></i></a></div>
			<div class="panel-body">

				<form id="coords" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-12">
							<p>Selecione uma área da foto e clique em Recortar.</p>

							<div class="cropper"></div>

							<div class="clearfix"><br /></div>
							<a onclick="crop()" class="btn btn-success icone"><i class="glyphicon glyphicon-scissors"></i> Recortar</a>
							<a onclick="crop_restore()" class="btn btn-danger icone aright"><i class="glyphicon glyphicon-picture"></i> Restaurar original</a>
						</div>

						<input type="hidden" id="x1" name="x1" />
						<input type="hidden" id="y1" name="y1" />
						<input type="hidden" id="x2" name="x2" />
						<input type="hidden" id="y2" name="y2" />
						<input type="hidden" id="w" name="w" />
						<input type="hidden" id="h" name="h" />
						<input type="hidden" id="crop_largura" name="crop_largura" />
						<input type="hidden" id="crop_altura" name="crop_altura" />
						<input type="hidden" id="crop_imgsrc" name="crop_imgsrc" />
						<input type="hidden" id="crop_aspect" name="crop_aspect" />
						<input type="hidden" id="crop_id" name="crop_id" />

					</div>
					<input type="hidden" value="" name="campo" />
					<input type="hidden" value="<?=$table?>" name="table" />
				</form>

			</div>
		</div>
	</div>
</div>


<!-- EDITAR GALERIA -->
<div id="galeria-edit" class="modal fade" role="dialog" data-backdrop="static">

	<div class="modal-dialog">
		<div class="panel panel-primary">
			<div class="panel-heading">Configurações da Imagem <a data-dismiss="modal" onclick="removeTiny()" class="fechar"><i class="glyphicon glyphicon-remove"></i></a></div>
			<div class="panel-body">
				<form method="POST" enctype="multipart/form-data">
					<div class="row">

						<div class="col-md-12">
							<label>
								<span>Descrição:</span>
								<div class="descricao-wrapper">
									<textarea id="tinyGaleria" name="descricao" class="form-control" autocomplete="off"></textarea>
								</div>
							</label>
						</div>

						<div class="col-md-12">
							<label>
								<span>Link:</span>
								<input class="form-control" name="link" type="text" value="" autocomplete="off" />
							</label>
						</div>

						<div class="col-md-12">
							<a onclick="salvar_config_galeria($(this)); removeTiny();" class="salvar btn btn-primary">SALVAR</a>
							<a data-dismiss="modal" class="salvar btn btn-danger aright" onclick="removeTiny()">CANCELAR</a>
						</div>
					</div>
					<input type="hidden" value="" name="id" />
				</form>
			</div>
		</div>
	</div>

</div>