$.ajaxSetup({ cache: false });

$(window).load(function () {
	$(".container").animate({"opacity": "1"}, 300);

	if(location.hash == "") {
		$(".escondido").animate({"opacity": 1});
	}
});


$(document).ready(function () {
	$(".menu-mobile").html("<ul><li><a class='abrir-mobile' status='fechado'><i class='zmdi zmdi-arrow-right'></i></a></li>" + $(".menu").html() + "</ul>");
	$(".abrir-mobile").click(function () {
		var maior = 0;
		$(".menu-mobile a span").each(function () {
			if($(this).width() > maior) { maior = $(this).width(); }
			$(this).find('i').after().text('');
		});
		var status = $(this).attr("status");
		if (status == "fechado") {
			$(".menu-mobile").stop().animate({
				width: maior + 100 + "vh",
				height: maior + 100 + "vh"
			})
			$(this).attr("status","aberto");
			$(".abrir-mobile i").removeClass("zmdi-arrow-right").addClass("zmdi-arrow-left");
		} else {
			if(status == "aberto") {
				$(".menu-mobile").stop().animate({
					width: "40px",
					height: "40px"
				})
				$(this).attr("status","fechado");
				$(".abrir-mobile i").removeClass("zmdi-arrow-left").addClass("zmdi-arrow-right");
			}
		}
	})

	$("a.fancybox").fancybox();

	$(".iconpicker").fontIconPicker({
	    theme: 'fip-bootstrap'
	});


	if($("#lista-estados").length > 0 && $("#lista-cidades").length > 0) {
		new dgCidadesEstados({
			estado: document.getElementById('lista-estados'),
			cidade: document.getElementById('lista-cidades')
		});
	}

	if($("#lista-estados").length > 0 && $("#lista-cidades").length == 0) {
		new dgCidadesEstados({
			estado: document.getElementById('lista-estados')
		});
	}

	if($("#lista-estados").length == 0 && $("#lista-cidades").length > 0) {
		new dgCidadesEstados({
			cidade: document.getElementById('lista-cidades')
		});
	}

	$(".salvar").click(function() {
		$('.modal').modal('hide');

	})

	$(".formulario").validador({
	    placeholder: false
	});

	if($(".table-configuracoes").length > 0) {
		$(".table-configuracoes select[name='tipo']").each(function() {
			var table = $(this).attr("table");
			var campo = $(this).attr("campo");
			var tipo = $(this).val();
			tipo_opcoes(table,campo,tipo,$(this));
		})

	}

	if($(".tinymce").length > 0) {
		tinymce.init({
		    selector: "textarea.tinymce",
			theme: "modern",
		    plugins: [
				"advlist autolink autosave link filemanager image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"table contextmenu directionality emoticons template textcolor paste textcolor"
		    ],

			toolbar1: "formatselect fontselect fontsizeselect | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | image media",
			toolbar2: "undo redo | link unlink | searchreplace | bullist numlist | outdent indent | table | hr | subscript superscript | charmap emoticons | print fullscreen preview | code",
			toolbar3: "",

			content_css: "css/tinymce.css",
		    relative_urls: true,
			menubar: false,
		    toolbar_items_size: 'small',
			language: 'pt_BR',
		    convert_urls: false,
		    remove_script_host : false

		});
	}

	$(".colorpicker").spectrum({
	    showInput: true,
	    className: "full-spectrum",
	    showInitial: true,
	    showPalette: true,
	    showSelectionPalette: true,
	    maxSelectionSize: 10,
	    preferredFormat: "hex",
	    palette: [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
    	]
	})

	if($(".table-visualizar").length > 0) {


		$(".tab-item").click(function() {
			ref = $(this).attr("href");
			location.hash = ref;
		})

		if(location.hash != "") {
			$(".tab-item[href="+location.hash+"]").tab('show');
			var func = $(".tab-item[href="+location.hash+"]").attr("onclick");
			if(typeof func != "undefined") {
				eval(func);
			}
		} else {
			tab = $("input[name=table]:first").val();
			exibir(null,$('#exibir_'+tab).val(),'0',null,null);
		}
	}


	//Busca
	$(".form-busca").each(function() {
		var f = $(this);
		var table_name = f.find("input[name=table]").val();

		var busca_campo_html = f.find(".lista-campos .campo:first").html();
		localStorage.setItem(table_name+"_busca_html",busca_campo_html);

		f.find(".lista-campos").html('');
		f.find(".lista-campos").append("<div class='campo' style='display: none;'>"+localStorage.getItem(table_name+"_busca_html")+"</div>");
		f.find(".lista-campos .campo:first").find(".remover-campo").remove();
		f.find(".lista-campos .campo:last").fadeIn();
		//f.find(".btn-buscar").click(function() { $("#form-busca").submit(); })

		$(this).find(".btn-buscar").click(function(e) {
			var t = $(this);
			e.preventDefault();

			var colunas = [];
			var termos = [];

			f.find("select[name=buscar_coluna]").each(function(index,element) {
				var valor = $(this).val();
				colunas.push(valor);
			});

			f.find("input[name=buscar_termo]").each(function(index,element) {
				var valor = $(this).val();
				termos.push(valor);
			});

			f.find("input[name="+table_name+"_colunas]").val(JSON.stringify(colunas));
			f.find("input[name="+table_name+"_termos]").val(JSON.stringify(termos));
			var tabela_externa = f.find("input[name=tabela_externa]").val();
			var id_externo = f.find("input[name=id_externo]").val();

			exibir(table_name,$('#exibir_'+table_name).val(),'0',id_externo,tabela_externa);
		})
	})

	//Indica que não foi salvo
	$(".crud input,.crud select").keyup(function() {
		$(".alteracoes").addClass("nao-salvas");
		$(".alteracoes .icone:first").hide();
		$(".alteracoes .icone:last").show();
	})

	$(".galeria").each(function() {
		var table = $(this).attr("tabela");
		var campo = $(this).attr("campo");
		$(this).sortable({
		  	stop: function( event, ui ) {
			  	atualiza_ordem_fotos($(this),table,campo);
			}
		});
	})



})

function atualiza_ordem_fotos(t,table,campo){
	ids = new Array();
	t.find("li").each(function(index, element) {
       ids.push($(element).attr("foto-id"));
    });

	$.ajax({
		url: 'tabela_acoes.php',
		type: 'POST',
		dataType: 'html',
		data: { acao: "atualizar_ordem", tabela: table, campo: campo, ids: ids.join(",") },
		success: function (data) {}
	})
}

function tipo_opcoes(table,campo,tipo,t) {

	if(tipo == "radio" || tipo == "checkbox") {
		tipo = "select";
	}

	if(tipo == "galeria") {
		tipo = "imagem";
	}

	t.parent().find(".icon-config").remove();

	if($("#tipo-"+tipo).length > 0) {
		t.after("<div data-toggle='modal' data-target='#tipo-"+tipo+"' onclick=\"informa_modal(\'"+tipo+"\',\'"+campo+"\',\'"+table+"\')\" class='icon-config'></div>");
	}
}

function informa_modal(tipo,campo,table) {
	reseta_modais();
	$("#tipo-"+tipo).find("input[name='campo']").val(campo);

	$.ajax({
		url: 'ajax/busca_config.php',
		type: 'POST',
		dataType: 'html',
		data: { table: table, campo: campo },
		success: function (data) {
			if(data != 0) {
				var resultado = data.split("|");

				if(tipo == "select" && resultado.length > 1) {
					desativa_campos($("#tipo-select .ativo"));
					ativa_campos($("#tipo-select .inativo"));
					$("#tipo-select .uncheck").prop("checked","checked");
					$("#tipo-select .check").removeAttr("checked");

					for(i = 0; i < resultado.length; i++) {
						$("#tipo-"+tipo).find("select[name='config"+(i+1)+"']").val(resultado[i]).parent().show();
						if(i == 1) { $(this).find(".opt").not("."+table).remove(); }
					}

				} else {

					for(i = 0; i < resultado.length; i++) {
						$("#tipo-"+tipo).find("input[name='config"+(i+1)+"']").val(resultado[i]);
					}

				}
			}
		}
	});
}

function salvar_config(t) {

	$.ajax({
		url: 'ajax/salvar_config.php',
		type: 'POST',
		dataType: 'html',
		data: t.closest("form").serialize(),
		success: function (data) {
		}
	});
}

function salvar_config_galeria(t) {
	$.ajax({
		url: 'ajax/salvar_config_galeria.php',
		type: 'POST',
		dataType: 'html',
		data: t.closest("form").serialize()+"&legenda="+escape(tinyMCE.get('tinyGaleria').getContent()),
		success: function (data) {
		}
	});
}

function editaDadosImg(id,t) {
	$("#galeria-edit input[name=id]").val(id);
	var descricao = t.parent().find(".galeria-descricao").html();
	var link = t.parent().find(".galeria-link").html();

	$("#galeria-edit textarea[name='descricao']").val(descricao);

	tinymce.init({
	    selector: "textarea#tinyGaleria",
		theme: "modern",
	    plugins: [
			"advlist autolink autosave link filemanager image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons template textcolor paste textcolor"
	    ],

		toolbar1: "formatselect fontselect fontsizeselect | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | image media",
		toolbar2: "undo redo | link unlink | searchreplace | bullist numlist | outdent indent | table | hr | subscript superscript | charmap emoticons | print fullscreen preview | code",
		toolbar3: "",

		content_css: "css/tinymce.css",
	    relative_urls: true,
		menubar: false,
	    toolbar_items_size: 'small',
		language: 'pt_BR',
	    convert_urls: false,
	    remove_script_host : false,
	    setup: function (ed) {
	        ed.on('init', function(args) {
	        	if($.trim(descricao) == "") {
					ed.setContent("");
				}
	        });
	    }

	});

	$("#galeria-edit input[name='link']").val(link);

}

function removeTiny() {
	var descricao = tinyMCE.get('tinyGaleria').getContent();
	tinymce.get("tinyGaleria").remove();
	var id = $("#galeria-edit").find("input[name=id]").val();

	$(".galeria .item[foto-id="+id+"] .galeria-descricao").html(descricao);
	$(".galeria .item[foto-id="+id+"] .galeria-link").html($("#galeria-edit input[name=link]").val());
}

function reseta_modais() {
	$("#tipo-select select[name=config2]").parent().hide();
	ativa_campos($("#tipo-select .ativo"));
	desativa_campos($("#tipo-select .inativo"));
	$("#tipo-select .check").prop("checked","checked");
	$("#tipo-select .uncheck").removeAttr("checked");
	$("#tipo-select select, #tipo-select input[type=text]").val("");

	$("#tipo-imagem input[name=config1],#tipo-imagem input[name=config2]").val('');

	$("#tipo-arquivo input[name=config1]").val('');
}

function pega_campos_select(tabela,t) {
	$.ajax({
		url: 'ajax/pega_campos_select.php',
		type: 'POST',
		dataType: 'html',
		data: { tabela: tabela },
		success: function (data) {
		    t.parent().next().fadeIn();
		    t.parent().next().find("select").html(data);
		}
	});
}

function ativa_campos(classe) {
	classe.find("input,select").not("input[type=radio],input[type=hidden]").removeAttr("disabled");
}

function desativa_campos(classe) {
	classe.find("input,select").not("input[type=radio],input[type=hidden]").attr("disabled","disabled");
}

function todos_editaveis() {

	var total = $("input[name='editavel']").length;
	var marcados = $("input[name='editavel']:checked").length;

	if(marcados <= (total/2)) {
		if(confirm("Deixar todos editáveis?")) {
			$("input[name='editavel']").prop("checked","checked");
			$("input[name='editavel']").each(function() {
				altera_campo($(this));
			})
		}
	} else {
		if(confirm("Deixar todos não-editáveis?")) {
			$("input[name='editavel']").removeAttr("checked");
			$("input[name='editavel']").each(function() {
				altera_campo($(this));
			})
		}
	}

}

function todos_visiveis() {
	var total = $("input[name='visivel']").length;
	var marcados = $("input[name='visivel']:checked").length;

	if(marcados <= (total/2)) {
		if(confirm("Deixar todos visíveis?")) {
			$("input[name='visivel']").prop("checked","checked");
			$("input[name='visivel']").each(function() {
				altera_campo($(this));
			})
		}
	} else {
		if(confirm("Deixar todos não-visíveis?")) {
			$("input[name='visivel']").removeAttr("checked");
			$("input[name='visivel']").each(function() {
				altera_campo($(this));
			})
		}
	}
}

function todos_obrigatorios() {
	var total = $("input[name='obrigatorio']").length;
	var marcados = $("input[name='obrigatorio']:checked").length;

	if(marcados <= (total/2)) {
		if(confirm("Deixar todos obrigatórios?")) {
			$("input[name='obrigatorio']").prop("checked","checked");
			$("input[name='obrigatorio']").each(function() {
				altera_campo($(this));
			})
		}
	} else {
		if(confirm("Deixar todos não-obrigatórios?")) {
			$("input[name='obrigatorio']").removeAttr("checked");
			$("input[name='obrigatorio']").each(function() {
				altera_campo($(this));
			})
		}
	}
}

function altera_campo(t) {
	var table = t.attr("table");
	var campo = t.attr("campo");
	var nome_config = t.attr("name");

	var config = "";

	if(t.attr("type") == "checkbox") {
		if(t.is(":checked")) {
			config = 1;
		} else {
			config = 0;
		}
	} else {
		config = t.val();
	}

	$.ajax({
		url: 'ajax/altera_campo_configuracao.php',
		type: 'POST',
		dataType: 'html',
		data: { table: table, campo: campo, nome_config: nome_config, config: config },
		success: function (data) {

			if(data == 1) {
			    if(t.attr("type") == "text") {
					t.next().fadeIn('fast');
					setTimeout(function() { t.next().fadeOut('fast') },1200);
				}
			} else {
				if(t.attr("type") == "text") {
					t.next().html("ERRO!");
					t.next().fadeIn('fast');
					setTimeout(function() { t.next().fadeOut('fast') },1200);
				}
			}
		}
	});
}


function informa_modal_crop(img,id,largura,altura) {
	var d = new Date();
	$(".cropper").html("<img src='uploads/"+img+"?"+d.getTime()+"' />");

	var jcrop_api;
	var aspect = Math.abs(largura/altura);

	$('.cropper img').Jcrop({
		aspectRatio: aspect,
		boxWidth: 500,
		boxHeight: 500,
		onChange:   showCoords,
		onSelect:   showCoords,
		onRelease:  clearCoords
	},function(){
		jcrop_api = this;
	});

	$('#coords').on('change','input',function(e){
		var x1 = $('#x1').val(),
		x2 = $('#x2').val(),
		y1 = $('#y1').val(),
		y2 = $('#y2').val();
		jcrop_api.setSelect([x1,y1,x2,y2]);
	});

	$("#crop_imgsrc").val(img);
	$("#crop_largura").val(largura);
	$("#crop_altura").val(altura);
	$("#crop_aspect").val(aspect);
	$("#crop_id").val(id);
}

function showCoords(c)
{
	$('#x1').val(c.x);
	$('#y1').val(c.y);
	$('#x2').val(c.x2);
	$('#y2').val(c.y2);
	$('#w').val(c.w);
	$('#h').val(c.h);
};

function clearCoords()
{
	$('#coords input').val('');
};

function deletar(t,table,id) {
	if(confirm("Tem certeza que deseja apagar este registro?")) {
		t.parent().parent().fadeOut('fast').remove();

		$.ajax({
			url: 'ajax/deletar.php',
			type: 'POST',
			dataType: 'html',
			data: { table: table, id:id },
			success: function (data) {

			}
		});

	}
}

function adicionar_campos(table,externa) {
	//$("#add_"+t).parent().before("<div class='campos'>"+$("#campos_"+t).html()+"</div>");

	$.ajax({
		url: 'ajax/adicionar_campos.php',
		type: 'POST',
		dataType: 'html',
		data: { table: table, externa: externa },
		success: function (data) {
			$("#add_"+table).parent().before("<div class='campos'>"+data+"</div>");

			$(".formulario").validador({
			    placeholder: false
			});
		}
	});

}

function removeImagem(t,table,id,campo) {

	if(confirm("Tem certeza que deseja remover esta imagem?")) {
		$.ajax({
			url: 'ajax/remover_imagem.php',
			type: 'POST',
			dataType: 'html',
			data: { table: table, id: id, campo:campo },
			success: function (data) {
				if(data == 1) {
					t.parent().fadeOut().remove();
				}
			}
		});
	}
}

function removeImagemGaleria(t,table,id,caminho) {

	if(confirm("Tem certeza que deseja remover esta imagem?")) {
		$.ajax({
			url: 'ajax/remover_imagem_galeria.php',
			type: 'POST',
			dataType: 'html',
			data: { table: table, id: id, caminho:caminho },
			success: function (data) {
				if(data == 1) {
					t.parent().fadeOut().remove();
				}
			}
		});
	}
}

function crop() {
	$.ajax({
		url: 'ajax/crop.php',
		type: 'POST',
		dataType: 'html',
		data: $("#crop-imagem form").serialize(),
		success: function (data) {
			var crop_id = $("#crop_id").val();
			var d = new Date();
			$('.modal').modal('hide');
			$("#"+crop_id).css("background-image","url('uploads/"+$("#crop_imgsrc").val()+'?'+d.getTime()+"')");

		}
	});
}

function crop_restore() {

	if(confirm("Tem certeza que deseja restaurar a imagem original?")) {
		$.ajax({
			url: 'ajax/crop_restore.php',
			type: 'POST',
			dataType: 'html',
			data: $("#crop-imagem form").serialize(),
			success: function (data) {
				var crop_id = $("#crop_id").val();
				var d = new Date();
				$('.modal').modal('hide');
				$("#"+crop_id).css("background-image","url('uploads/"+$("#crop_imgsrc").val()+'?'+d.getTime()+"')");

			}
		});
	}
}

function ordenacao_iniciar(table,id) {
	tab = table;
	$(".cancelar-ordenacao").fadeIn();
	$(".ordenar-spot").off("click");

	$(".ordenar-spot").fadeIn('fast',function() {
		if($("#linha-"+id).length != 0) {
			$(document).data("ordenar",id);
			$(document).data("ordenar_html",$("#linha-"+id).html());
		}

		$(".cancelar-ordenacao").click(function() {
			$(document).removeData("ordenar");
			$(document).removeData("ordenar_html");
			$(".ordenar-spot").fadeOut('fast');
			$(".cancelar-ordenacao").fadeOut();

			$(".cancelar-ordenacao").off("click");
			$(".ordenar-spot").off("click");
		})
	});

	$(".ordenar-spot").click(function() {
		var ordenar_html = $(document).data("ordenar_html");
		var origem = $(document).data("ordenar");
		var tr_html = "<tr id='linha-"+origem+"'>"+ordenar_html+"</tr>";
		var destino = "";
		$(document).removeData("ordenar");
		$(document).removeData("ordenar_html");

		$("#linha-"+id).addClass("remover-linha").fadeOut('fast');

		//Verifica se não é a última linha
		if(typeof $(this).parent().parent().attr("id") != "undefined") {
			var destino = $(this).parent().parent().attr("id")+"-antes";
			$(this).parent().parent().before(tr_html);
		} else {
			var destino = $(this).prev().find("tr:last").attr("id")+"-depois";
			$(this).prev().find("tr:last").after(tr_html);
		}
		$(".remover-linha").remove();

		$(document).removeData("ordenar");
		$(document).removeData("ordenar_html");
		$(".ordenar-spot").fadeOut('fast');
		$(".cancelar-ordenacao").fadeOut();

		$(".cancelar-ordenacao").off("click");
		$(".ordenar-spot").off("click");

		destino = destino.replace("linha-","");
		$.ajax({
			url: 'ajax/ordenar.php',
			type: 'POST',
			dataType: 'html',
			data: { table: table, posicao_original: origem, posicao_nova: destino },
			success: function (data) {
				exibir(table,$('#exibir_'+table).val(),$(".pagination_"+table+" .active").index(),null,null);
			}
		});

	})
}

function exibir(table,v,pag,id_externo,table_externo) {

	if(typeof table == "undefined" || table == null) {
		table = $("input[name=table]:first").val();
	}

	if(typeof v != "undefined") {
		setCookie("exibir_"+table, v);
	}

	var colunas = $("."+table+"_colunas").val();
	var termos = $("."+table+"_termos").val();
	var nivel = $(".nivel").val();
	var ordem_campo = "";
	var ordem_modo = "";
	
	if(localStorage.getItem(table+"_ordem_campo")) {
		//ordem_campo = localStorage.getItem(table+"_ordem_campo");
	}
	if(localStorage.getItem(table+"_ordem_modo")) {
		//ordem_modo = localStorage.getItem(table+"_ordem_modo");
	}
	if(typeof colunas != "undefined" && colunas != "") { colunas = JSON.parse(colunas); }
	if(typeof termos != "undefined" && termos != "") { termos = JSON.parse(termos); }

	$(".visualizar-"+table+" tbody").fadeOut('fast');
	$.ajax({
		url: 'ajax/exibir.php',
		type: 'POST',
		dataType: 'html',
		data: { table: table, exibir: v, pag: pag, nivel:nivel, id_externo: id_externo, table_externo: table_externo, colunas: colunas, termos: termos, ordem_campo: ordem_campo, ordem_modo: ordem_modo },
		success: function (data) {
			resp = data.split("<paginacao />");

			$(".visualizar-"+table+" .table").html(resp[0]);
			$(".pagination_"+table).html(resp[1]);

			$(".visualizar-"+table+" tbody").fadeIn('fast');

			//verifica se está ordenando
			if(typeof $(document).data("ordenar") != "undefined") {
				ordenacao_iniciar(table,$(document).data("ordenar"));
			}

			$("#exibir_"+table).attr("onchange",resp[2]);

			$(".escondido").animate({"opacity":1});
			if($.trim($(".pagination").html()) == "") { $(".pagination").hide(); } else { $(".pagination").show(); }

			$(".table-visualizar thead td").each(function() {
				if($.trim($(this).text()) == "#") {
					$(this).css("white-space","nowrap");
					$(this).width(1);
				}
			})

			//Ordenar por titulo
			$(".ordenar-titulo").click(function() {
				var campo = $(this).attr("campo");
				var modo = $(this).attr("modo");
				var tabela = $(this).attr("tabela");

				if($(".pagination_"+tabela+" li").length > 0) {
					var func = $(".pagination_"+tabela).find(".active").find("a").attr("onclick");
				} else {
					var func = $("#exibir_"+tabela).attr("onchange");
				}

				localStorage.setItem(tabela+"_ordem_campo",campo);
				localStorage.setItem(tabela+"_ordem_modo",modo);
				eval(func);
			})

		}
	});
}





function adicionarBusca(table_name) {
	$("#buscar_"+table_name+" .lista-campos").append("<div class='campo' style='display: none;'>"+localStorage.getItem(table_name+"_busca_html")+"</div>");
	$("#buscar_"+table_name+" .lista-campos .campo:first-child").find(".remover-campo").remove();
	$("#buscar_"+table_name+" .lista-campos .campo:last-child").slideDown();
}

function removerBusca(t) {
	t.parent().parent().animate({ height: 0 },function() { $(this).remove(); });
}

function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}
