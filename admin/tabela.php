<?
if(!isset($tabela_externa)) {
	$table = $_GET["t"];
} else {
	$table = $tabela_externa;
}

$acesso = true;
if(!tem_permissao($table,'acessar')) {
	?><h2><i class="zmdi zmdi-block-alt"></i> Acesso negado</h2><?
	$acesso = false;
}

?>
<input type="hidden" name="table" value="<?=$table?>" />

<?php
$isSecure = false;
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
	$isSecure = true;
}
elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
	$isSecure = true;
}
$REQUEST_PROTOCOL = $isSecure ? 'https' : 'http';
if(isset($_GET["nivel"])) { $nivel = $_GET["nivel"]; } else { $nivel = '0'; }
$_SESSION['nivel'][$nivel] = $REQUEST_PROTOCOL."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];


if(isset($_GET["id"])) {
	$id = $_GET["id"];
} else {
	$id = null;
}

$nome_table = normalizaString($table);

$sql_config = mysqli_query($GLOBALS["db"], "SELECT * FROM admin_configuracoes WHERE tabela = '".$table."' and campo = ''");
if(mysqli_num_rows($sql_config) != 0) {
	$config = mysqli_fetch_array($sql_config);

	if($config["um_registro"] == 1) {
		$get_last = mysqli_query($GLOBALS["db"], "SELECT * FROM $table ORDER BY id DESC LIMIT 1");

		if(mysqli_num_rows($get_last) != 0) {
			$ide = mysqli_result($get_last);
			$link = "index.php?pag=tabela_campos&t=".$table."&id=".$ide."&nivel=1";
			header("Location: ".$link);
		}
	}

	$editavel = $config["editavel"];
	if($config["label"] != "") { $nome_table = $config["label"]; }

	$orderby = "id ASC";
	if(possui_ordem($table)) { $orderby = "admin_ordem ASC"; }
	if(isset($config["orderby"]) and $config["orderby"] != "") { $orderby = $config["orderby"]; }

}

if(isset($nivel) and $nivel == 0) { $_SESSION["bread"][0] = $nome_table; }

if($acesso) {
?>

<h2>
	<? if($config["icone"]) { ?><i class="<?=$config["icone"]?>"></i><? } ?>
	<?=retira_prefixo($nome_table)?>
</h2>


<? if(tem_permissao($table,'cadastrar')) { ?>
	<a class="btn btn-primary icone aright btn-top" href="index.php?pag=tabela_campos&t=<?=$table?><? if(isset($_GET["t"]) and $_GET["t"] != "") { ?>&table_externo=<?=$_GET["t"]?><? } ?><? if(isset($_GET["id"]) and $_GET["id"] != "") { ?>&id_externo=<?=$_GET["id"]?><? } ?><? if(@$_GET["nivel"] != "") { echo "&nivel=".($_GET["nivel"]+1); } else { echo "&nivel=1"; } ?>"><i class="glyphicon glyphicon-plus"></i> ADICIONAR</a>
	<?

	if(isset($_GET["t"]) and $_GET["t"] == "newsletter") {
		?>
		<div class="clearfix"></div>

		<p>Copie a lista abaixo e cole no campo "Para:" do seu gerenciador de e-mail favorito.</p>
		<textarea class="form-control" style="height: 80px"><?php
			$sql = mysqli_query($GLOBALS['db'],"SELECT * FROM newsletter ORDER BY data_cadastro DESC");
			while($news = mysqli_fetch_array($sql)) {
				echo $news["email"].";";
			}
			?></textarea><br />
		<?php
	}
	?>
	<?
	//DUPLICAR
	if(strpos($table,"em_uso") !== false) { ?>
		<?
		global $dbname;
		$table_em_uso = str_replace("_em_uso","",$table);
		$em_uso_sql = mysqli_query($GLOBALS["db"], "select table_name from information_schema.columns where table_schema = '$dbname' and table_name = '$table_em_uso'") or die(mysqli_error($GLOBALS["db"]));
		if(mysqli_num_rows($em_uso_sql) > 0) {
			?>
			<? include("replicar.php"); ?>

		<? } ?>
	<? } ?>

<? } ?>

<div class="clearfix"></div>

<?
if(isset($_GET["p"])) {
	$pagina_atual = $_GET["p"];
} else {
	$pagina_atual = 0;
}
?>


<? if(tem_permissao($table,'visualizar')) { ?>

<div class="panel panel-primary">
	<div class="panel-heading config-exibir">
		<div class="cancelar-ordenacao">
			Clique aqui para CANCELAR a ordenação deste registro.
		</div>
		<div class="row">
			<div class="col-md-6">
				Exibir por página:

				<select id="exibir_<?=$table?>" onChange="">
					<?php
					$total_sql = mysqli_query($GLOBALS["db"], "SELECT id FROM $table");
					$total = mysqli_num_rows($total_sql);
					$intervalo = 25;
					$maximo = 201;
					$minimo = 25;

					if(isset($_COOKIE["exibir_".$table])) { $exibir_table = $_COOKIE["exibir_".$table]; } else { $exibir_table = 25; }

					for($i = $minimo; $i <= $maximo; $i=$i+$intervalo) {
						if($i != $total) {
							?><option value="<?=$i?>" <? if($exibir_table == $i) { echo "selected"; } ?>><?=$i?></option><?
						}
					}
					?>
					<option <? if($exibir_table == 'todos') { echo "selected"; } ?> value="todos">Todos</option>
				</select>
			</div>
			<div class="col-md-6">
				<a data-toggle='modal' data-target='#buscar_<?=$table?>' class="btn btn-xs" href=""><i class="glyphicon glyphicon-search"></i></a>
			</div>
		</div>
	</div>
	<div class="panel-body" style="padding: 0px">
		<div class="table-visualizar visualizar-<?=$table?> table-responsive">
			<table class="table table-striped">

				<?php

				$sql_config = mysqli_query($GLOBALS["db"], "SELECT campo,label FROM admin_configuracoes WHERE tabela = '".$table."' and visivel = 1");
				if(mysqli_num_rows($sql_config) != 0) {
					$campos_visiveis = array();
					?>


				<? } else { ?>
					<tbody>
					<tr>
						<td>Nenhum campo foi configurado como visível.</td>
					</tr>
					</tbody>
				<? } ?>

			</table>
			<div class="ordenar-spot"><i class="glyphicon glyphicon-triangle-right"></i><span></span></div>
		</div>
	</div>
</div>

<ul class="pagination pagination_<?=$table?>"></ul>

<!-- BUSCA -->
<div id="buscar_<?=$table?>" class="buscar modal fade" role="dialog" data-backdrop="static">

	<div class="modal-dialog">
		<div class="panel panel-primary">
			<div class="panel-heading">Buscar<a data-dismiss="modal" class="fechar"><i class="glyphicon glyphicon-remove"></i></a></div>
			<div class="panel-body">

				<form class="form-busca" method="POST" enctype="multipart/form-data">
					<div class="row">

						<div class="lista-campos">

							<div class="campo">

								<div class="col-md-1">
									<a onclick="removerBusca($(this))" class="remover-busca btn btn-danger btn-xs"> <i class="glyphicon glyphicon-remove"></i> </a>
								</div>
								<div class="col-md-5">
									<label>
										<span>Buscar na coluna:</span>

										<select name="buscar_coluna" class="form-control">
											<optgroup label="<?=$nome_table?>">
												<?php
												$sql = "show full columns from $table";
												$result = mysqli_query($GLOBALS["db"],$sql) or die(mysqli_error($GLOBALS["db"]));
												while($col = mysqli_fetch_array($result)) {

													$sql_config = mysqli_query($GLOBALS["db"], "SELECT campo,label FROM admin_configuracoes WHERE tabela = '".$table."' and campo = '".$col["Field"]."' and pesquisavel = 1");
													if(mysqli_num_rows($sql_config) != 0) {
														while($config = mysqli_fetch_array($sql_config)) {
															if($config["label"] != "") { $nome = $config["label"]; } else {
																$nome = normalizaString($config["campo"]);
															}
															?>
															<option value="<?=$table?>.<?=$config["campo"]?>"><?=$nome?></option>
														<? } ?>
													<? } ?>

												<? } ?>
											</optgroup>

											<!-- Campos Externos -->
											<?
											global $dbname;
											$externo_sql = mysqli_query($GLOBALS["db"], "select table_name from information_schema.columns where table_schema = '$dbname' and column_name = 'id_".$table."'") or die(mysqli_error($GLOBALS["db"]));
											if(mysqli_num_rows($externo_sql) != 0) {
												while($externo = mysqli_fetch_array($externo_sql)) {

													$nome_table = normalizaString($externo["table_name"]);
													$config_table = mysqli_query($GLOBALS["db"], "SELECT * FROM admin_configuracoes WHERE tabela = '".$externo["table_name"]."' and campo = ''");
													$aba_editavel = 0;
													if(mysqli_num_rows($config_table) != 0) {
														$tb_config = mysqli_fetch_array($config_table);
														if($tb_config["label"] != "") { $nome_table = $tb_config["label"]; }
														$aba_editavel = $tb_config["aba_editavel"];
													}

													if($aba_editavel) {
														?>

														<optgroup label="<?=$nome_table?>">
															<?
															$sql = "show full columns from ".$externo["table_name"];
															$result = mysqli_query($GLOBALS["db"],$sql) or die(mysqli_error($GLOBALS["db"]));
															while($col = mysqli_fetch_array($result)) {

																$sql_config = mysqli_query($GLOBALS["db"], "SELECT campo,label FROM admin_configuracoes WHERE tabela = '".$externo["table_name"]."' and campo = '".$col["Field"]."' and pesquisavel = 1");
																if(mysqli_num_rows($sql_config) != 0 and $aba_editavel) {
																	?>
																	<?php
																	while($config = mysqli_fetch_array($sql_config)) {
																		if($config["label"] != "") { $nome = $config["label"]; } else {
																			$nome = normalizaString($config["campo"]);
																		}
																		?>
																		<option value="<?=$externo["table_name"]?>.<?=$config["campo"]?>"><?=$nome?></option>
																	<? } ?>

																<? } ?>

															<? } ?>
														</optgroup>
													<? } ?>

												<? } ?>
											<? } ?>


										</select>

									</label>
								</div>

								<div class="col-md-6">
									<label>
										<span>Termo buscado:</span>
										<input name="buscar_termo" type="text" class="form-control" />
									</label>
								</div>
								<div class="clearfix"></div>
							</div>

						</div>

						<div class="col-md-12">
							<hr />
							<a onclick="adicionarBusca('<?=$table?>')" class="adicionar-busca btn btn-success icone"><i class="glyphicon glyphicon-plus"></i> ADICIONAR BUSCA</a>
							<a data-dismiss="modal" class="btn-buscar btn btn-primary icone aright"><i class="glyphicon glyphicon-search"></i> BUSCAR</a>
						</div>

					</div>
					<input type="hidden" value="" name="campo" />
					<input type="hidden" value="<?=$table?>" name="table" />
					<input type="hidden" value='' name="<?=$table?>_colunas" class="<?=$table?>_colunas" />
					<input type="hidden" value='' name="<?=$table?>_termos" class="<?=$table?>_termos" />
					<? if(isset($tabela_externa)) { ?><input type="hidden" value="<?=$_GET["t"]?>" name="tabela_externa" /><? } ?>
					<? if(isset($_GET["id"])) { ?><input type="hidden" value="<?=$_GET["id"]?>" name="id_externo" /><? } ?>
				</form>

			</div>
		</div>
	</div>
	<? } ?>

	<? } ?>
</div>
