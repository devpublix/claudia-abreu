	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/cidades-estados-1.2-utf8.js"></script>
	<script type="text/javascript" src="js/validador.js"></script>
	<script type="text/javascript" src="js/jcrop.min.js"></script>
	<script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript" src="js/colorpicker.js"></script>
	<script type="text/javascript" src="js/sweetalert.min.js"></script>
	<script type="text/javascript" src="js/iconpicker.js"></script>
	<script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
	<!-- <script type="text/javascript" src="js/instafeed.js"></script> -->
	<script type="text/javascript" src="js/scripts.js"></script>

	<script type="text/javascript">
		<? if(isset($_GET["msg"]) and $_GET["msg"] == "sucesso") { ?>
			
			<? if($um_registro) { ?>

				swal({
				    title: 'Pronto!',
				    text: "Informações salvas com sucesso!",
				    type: 'success',
				    showCancelButton: false,
				    closeOnConfirm: true,
				    showLoaderOnConfirm: false
			  	}, function(){
				    var actual_url = $("#actual_url").val();
				    if(location.hash != "") { actual_url += location.hash; }
				    //actual_url = actual_url.replace("&msg=sucesso","");
				    //location.href=actual_url;
			 	});

			<? } else { ?>

				swal({
					title: "Informações Salvas!",
					text: "Deseja voltar para a listagem?",
					type: "success",
					showCancelButton: true,
					confirmButtonColor: '#82c15f',
					confirmButtonText: "Sim",
					cancelButtonText: "Não",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function(isConfirm){
			    if (isConfirm){
			      location.href=$("#btn-voltar").attr("href");
			    } else {
			      //var actual_url = $("#actual_url").val();
				  //if(location.hash != "") { actual_url += location.hash; }
				  //actual_url = actual_url.replace("&msg=sucesso","");
				  //location.href=actual_url;
			    }
				});
			<? } ?>

		<? } ?>

		<? if(isset($_GET["msg"]) and $_GET["msg"] == "erro") { ?>
			swal({
			    title: 'Erro!',
			    text: "Entre em contato com o administrador para reportar o erro.",
			    type: 'error',
			    showCancelButton: false,
			    closeOnConfirm: true,
			    showLoaderOnConfirm: false
		  	}, function(){
			    var actual_url = $("#actual_url").val();
			    if(location.hash != "") { actual_url += location.hash; }
			    actual_url = actual_url.replace("&msg=erro","");
			    location.href=actual_url;
		 	});

		<? } ?>

		<? if(isset($_GET["msg"]) and $_GET["msg"] == "inexistente") { ?>
			swal({
			    title: 'Dados incorretos!',
			    text: "Usuário/senha incorretos.",
			    type: 'error',
			    showCancelButton: false,
			    closeOnConfirm: true,
			    showLoaderOnConfirm: false
		  	})

		<? } ?>

		<? if(isset($_GET["msg"]) and $_GET["msg"] == "entrar") { ?>
			swal({
			    title: 'Seja bem vindo(a)!',
			    imageUrl: 'img/logo.png',
			    showCancelButton: false,
			    closeOnConfirm: true,
			    showLoaderOnConfirm: false
		  	})


		<? } ?>

		//Menu active
		$(".menu li a[href='index.php?<? if(isset($_SESSION['nivel'][0])) { $exp = explode('?',$_SESSION['nivel'][0]); echo end($exp); } ?>']").addClass("active");
	</script>

</body>
</html>